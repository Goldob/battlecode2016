### About ###
This is team002's entry for [Battlecode 2016](http://www.battlecode.org/) competition. Each package represents one bot, with main being team002.

Game specs are available [here](http://s3.amazonaws.com/battlecode-releases-2016/releases/specs-0.2.2.htmlLink URL).