//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint;

import _sprint.agents.ArchonAgent;
import _sprint.agents.GuardAgent;
import _sprint.agents.ScoutAgent;
import _sprint.agents.SoldierAgent;
import _sprint.agents.TurretAgent;
import _sprint.agents.ViperAgent;
import battlecode.common.Clock;
import battlecode.common.RobotController;
import battlecode.common.RobotType;

public class RobotPlayer {
	
	public static void run(RobotController controller) {
		Agent agent = craftAgent(controller.getType());
		
		agent.init(controller);
		while(true) {
			agent.nextRound();
			Clock.yield();
		}
	}
	
	private static Agent craftAgent(RobotType type) {
		switch (type) {
		case ARCHON:
			return new ArchonAgent();
		case GUARD:
			return new GuardAgent();
		case SCOUT:
			return new ScoutAgent();
		case SOLDIER:
			return new SoldierAgent();
		case TTM:
		case TURRET:
			return new TurretAgent();
		case VIPER:
			return new ViperAgent();
		default:
			return null;
		}
	}
}
