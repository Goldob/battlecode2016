//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.agents;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;

import static _sprint.modules.Movement.MovementConstraint.*;
import static _sprint.utils.Locations.*;
import static _sprint.utils.Utils.*;
import static battlecode.common.RobotType.ARCHON;

import _sprint.Agent;
import _sprint.modules.Movement.MovementMode;

public class ArchonAgent extends Agent {
	protected Behavior BUILD_AND_WANDER;
	protected Behavior ACTIVATE_UNITS;
	protected Behavior PANIC;
	
	private RobotInfo[] neutralRobots;
	
	private boolean fixedSomething;
	
	@Override
	public void init(RobotController controller) {
		super.init(controller);
		BUILD_AND_WANDER = new Behavior() {
			
			@Override
			public void whenCoreReady() {
				RobotType construction = getNextConstruction();
				
				if(controller.hasBuildRequirements(construction)) {
					tryToBuild(construction);
				} else {
					MapLocation[] partLocations = controller.sensePartLocations(-1);
					if(partLocations.length == 0 || !getMovement().tryToMove(getController().getLocation().
							directionTo(partLocations[0]), MovementMode.FORWARD)) {
						WANDER.whenCoreReady();
					}
				}
			}

			@Override
			public void whenWeaponReady() {}

			@Override
			public void whenBothReady() {
				whenCoreReady();
			}
		};
		
		getController().addMatchObservation("This is a test");
		
		ACTIVATE_UNITS = new Behavior() {

			@Override
			public void whenCoreReady() {
				RobotInfo closest = getClosestRobot(getController().getLocation(), 
						controller.senseNearbyRobots(ARCHON.sensorRadiusSquared, Team.NEUTRAL));
				if(closest != null) {
					if(getController().getLocation().isAdjacentTo(closest.location)) {
						tryToActivate(closest.location);
					} else {
						getMovement().tryToMove(getController().getLocation()
								.directionTo(closest.location), MovementMode.BUG, AVOID_FOOTSTEPS);
					}
				}
			}
			
			@Override
			public void whenWeaponReady() {}

			@Override
			public void whenBothReady() {
				whenCoreReady();
			}
		};
		
		PANIC = new Behavior() {
			
			@Override
			public void whenCoreReady() {
				MapLocation location = controller.getLocation();
				Direction toEnemies = location.directionTo(average(extractLocations(getHostiles())));
				
				if(toEnemies == Direction.OMNI)
					toEnemies = getDirection(getRandom().nextInt(8));
				
				if(!getMovement().tryToMove(toEnemies.opposite(), MovementMode.FORWARD, AVOID_RUBBLE) &&
						!getMovement().tryToMove(toEnemies.opposite(), MovementMode.FORWARD)) {
					int zombieCount = controller.senseNearbyRobots(-1, Team.ZOMBIE).length;
					if(zombieCount/2 >= getHostiles().length - zombieCount) {
						if(getController().hasBuildRequirements(RobotType.GUARD))
							tryToBuild(RobotType.GUARD, toEnemies);
					} else if(getController().hasBuildRequirements(RobotType.SOLDIER))
						tryToBuild(RobotType.SOLDIER, toEnemies);
				}
			}
			
			@Override
			public void whenWeaponReady() {}

			@Override
			public void whenBothReady() {
				whenCoreReady();
			}
		};
	}
	
	@Override
	public void onTurnBegin() {
		super.onTurnBegin();
		
		neutralRobots = getController().senseNearbyRobots(-1, Team.NEUTRAL);
		
		fixedSomething = tryToFixSomething();
	}
	
	@Override
	public void onTurnEnd() {
		super.onTurnEnd();
		
		if(!fixedSomething) tryToFixSomething();
	}
	
	private RobotType getNextConstruction() {
		// TODO
		return RobotType.SOLDIER;
	}
	
	@Override
	public Behavior makeDecisions() {
		if(getHostiles().length > 0) {
			return PANIC;
		} else if(neutralRobots.length > 0) {
			return ACTIVATE_UNITS;
		} else {
			return BUILD_AND_WANDER;
		}
	}
	
	protected boolean tryToFixSomething() {
		double maxDifference = 0.0;
		RobotInfo mostBroken = null;
		
		for (RobotInfo robot : getController().senseNearbyRobots(
				ARCHON.attackRadiusSquared, getController().getTeam())) {
			if (robot.type != RobotType.ARCHON && robot.maxHealth - robot.health > maxDifference) {
				mostBroken = robot;
				maxDifference = robot.maxHealth-robot.health;
			}
		}
		
		if(maxDifference > 0) {
			try {
				getController().repair(mostBroken.location);
				return true;
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	protected boolean tryToActivate(MapLocation location) {
		try {
			getController().activate(location);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	protected boolean tryToBuild(RobotType construction, Direction direction) {
		Direction[] offsetDirections = offsetDirection(direction, new int[]{0,-1,1,-2,2,-3,3,-4});
		
		for(Direction offsetDirection : offsetDirections) {
			if(getController().canBuild(offsetDirection, construction)) {
				try {
					getController().build(offsetDirection, construction);
					return true;
				} catch (GameActionException e) {
					e.printStackTrace();
				}
			}
		}
		
		return false;
	}
	
	protected boolean tryToBuild(RobotType construction) {
		return tryToBuild(construction, getDirection(getRandom().nextInt(8)));
	}
}
