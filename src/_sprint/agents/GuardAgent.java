//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.agents;

import _sprint.Agent;

public class GuardAgent extends Agent {
	
	@Override
	public Behavior makeDecisions() {
		if(getHostiles().length > 0) {
			return SHOOT;
		} else if(getController().getLocation().
				distanceSquaredTo(getMotherArchon().location) > 50) {
			return FOLLOW_LEADER;
		}
		
		return WANDER;
	}
}
