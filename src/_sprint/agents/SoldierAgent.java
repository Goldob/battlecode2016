//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.agents;

import _sprint.Agent;
import battlecode.common.RobotInfo;
import battlecode.common.Team;

public class SoldierAgent extends Agent {	
	
	@Override
	public Behavior makeDecisions() {
		if(getHostiles().length > 0) {
			return SHOOT;
		} else if(getController().getLocation().
				distanceSquaredTo(getMotherArchon().location) > 50) {
			return FOLLOW_LEADER;
		}
		
		return WANDER;
	}
	
	@Override
	public boolean compareTargets(RobotInfo first, RobotInfo second) {
		if(first.team == Team.ZOMBIE && second.team != Team.ZOMBIE) {
			return first.health > second.health * 2;
		} else if(first.team != Team.ZOMBIE && second.team == Team.ZOMBIE) {
			return first.health * 2 > second.health;
		} else return first.health > second.health;
	}
}
