//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.agents;

import _sprint.Agent;
import battlecode.common.GameActionException;
import battlecode.common.RobotType;

public class TurretAgent extends Agent {

	@Override
	public Behavior makeDecisions() {
		// TODO
		if(getController().getType() == RobotType.TURRET) {
			return SHOOT;
		} else {
			return new Behavior() {
				
				@Override
				public void behave() {
					try {
						getController().unpack();
					} catch (GameActionException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void whenCoreReady() {}

				@Override
				public void whenWeaponReady() {}

				@Override
				public void whenBothReady() {}
			};
		}
	}
	
	@Override
	public boolean tryShootSomething(boolean engage) {
		// Turrets don't move, silly. And we don't want them to be unarmed TTM during combat
		return super.tryShootSomething(false);
	}
}
