//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.agents;

import _sprint.Agent;

public class ViperAgent extends Agent {

	@Override
	public Behavior makeDecisions() {
		if(getHostiles().length > 0) {
			return SHOOT;
		}
		
		return WANDER;
	}
}
