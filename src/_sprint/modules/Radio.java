//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.modules;

import static _sprint.utils.Utils.*;

import _sprint.Agent;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.Signal;

public class Radio {
	private final static int RECEIVE_COUNT = 20;
	
	private Agent agent;
	
	public Radio(Agent agent) {
		this.agent = agent;
	}
	
	public void listen() {
		RobotController controller = agent.getController();
		for(int i = 0; i < RECEIVE_COUNT; i++) {
			Signal signal = controller.readSignal();
			if(signal == null)
				return;
			
			processSignal(signal);
		}
	}
	
	public void broadcast(EncodedMessage encodedMessage, int radius) {
		try {
			if(encodedMessage.getType() == MessageType.DEFAULT) {
				agent.getController().broadcastSignal(radius);
			} else {
				int[] message = encodedMessage.toIntArray();
				agent.getController().broadcastMessageSignal(message[0], message[1], radius);
			}
		} catch(GameActionException e) {
			e.printStackTrace();
		}
	}
	
	private void processSignal(Signal signal) {
		if(signal.getTeam() == agent.getController().getTeam()) {
			EncodedMessage encodedMessage = new EncodedMessage(signal.getMessage());
			
			switch(encodedMessage.getType()) {
			case DEFAULT:
				break;				
			}
		} else {
			// TODO agent.getRecorder().recordEnemySignal(signal);
		}
	}
	
	/**
	 * Quite a hacky class for encoding and decoding signal messages.
	 * Indexes refer to bit positions and the last 16 are reserved for metadata.
	 */
	public static class EncodedMessage {
		private long content;
		
		public EncodedMessage(MessageType type) {
			content = 0;
			setByte(48, (byte) type.ordinal());
		}
		
		public EncodedMessage(int[] halfs) {
			if (halfs != null) {
				content = combine(halfs[0], halfs[1]);
			} else {
				content = 0;
			}
		}
		
		public int[] toIntArray() {
			return split(content);
			
		}
		
		public MessageType getType() {
			return MessageType.values()[getByte(48)];
		}
		
		public byte getByte(int index) {
			return (byte)((content >> index) & 0xFF);
		}
		
		public void setByte(int index, byte value) {
			store(index, value, 0xFF);
		}
		
		public short getShort(int index) {
			return (short)((content >> index) & 0xFFFF);
		}
		
		public void setShort(int index, short value) {
			store(index, value, 0xFFFF);
		}
		
		public MapLocation getMapLocation(int index) {
			return new MapLocation(getShort(index) - 16000, getShort(index + 16) - 16000);
		}
		
		public void setMapLocation(int index, MapLocation location) {
			setShort(index, (short)(location.x + 16000));
			setShort(index + 16, (short)(location.y + 16000));
		}
		
		private void store(int index, long value, long mask) {
			content = (content & ~(mask << index)) | (value << index);
		}
	}
	
	public static enum MessageType {
		DEFAULT,
		
		// Scouting
		
		MAP_BORDER_DETECTED,
		ZOMBIE_DEN_DETECTED,
		
	}
}
