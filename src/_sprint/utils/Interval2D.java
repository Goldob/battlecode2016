//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.utils;

import battlecode.common.MapLocation;

public class Interval2D {
	public static final int NO_INCLUSION = 0;
	public static final int PARTIAL_INCLUSION = 1;
	public static final int FULL_INCLUSION = 2;
	
	
	private MapLocation nw;
	private MapLocation se;
	
	public Interval2D(MapLocation from, MapLocation to) {
		nw = new MapLocation(Math.min(from.x, to.x), Math.min(from.y, to.y));
		se = new MapLocation(Math.max(from.x, to.x), Math.max(from.y, to.y));
	}
	
	public boolean contains(MapLocation location) {
		return location.x >= nw.x 
				&& location.x <= se.x 
				&& location.y >= nw.y
				&& location.y <= se.y;
	}
	
	public MapLocation getNWCorner() {
		return nw;
	}
	
	public MapLocation getSECorner() {
		return se;
	}
	
	public int getWidth() {
		return se.x - nw.x + 1;
	}
	
	public int getHeight() {
		return se.y - nw.y + 1;
	}
	
	public int getInclusion(Interval2D interval) {
		MapLocation nw2 = interval.nw;
		MapLocation se2 = interval.se;
		
		if(contains(nw2) && contains(se2))
			return FULL_INCLUSION;
		
		if(nw.x > se2.x || nw.y > se2.y || se.x < nw2.x || se.y < nw2.y)
			return NO_INCLUSION;
		
		return PARTIAL_INCLUSION;
	}
}
