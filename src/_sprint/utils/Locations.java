//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.utils;

import java.util.ArrayList;
import java.util.List;

import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;

public class Locations {
	private Locations() {};
	
	public static MapLocation add(MapLocation a, MapLocation b) {
		return a.add(b.x, b.y);
	}
	
	public static MapLocation substract(MapLocation a, MapLocation b) {
		return a.add(-b.x, -b.y);
	}
	
	public static MapLocation setX(MapLocation location, int x) {
		return new MapLocation(x, location.x);
	}
	
	public static MapLocation setY(MapLocation location, int y) {
		return new MapLocation(location.x, y);
	}
	
	public static MapLocation average(MapLocation[] locations) {
		int x, y, n;
		x = y = n = 0;
		for(MapLocation location : locations) {
			x += location.x;
			y += location.y;
			n++;
		}
		return new MapLocation(x/n, y/n);
	}
	
	public static MapLocation[] extractLocations(RobotInfo[] robots) {
		MapLocation[] locations = new MapLocation[robots.length];

		for(int i = 0; i < robots.length; i++) {
			locations[i] = robots[i].location;
		}
		
		return locations;
	}
}
