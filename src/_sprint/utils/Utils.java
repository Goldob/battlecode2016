//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package _sprint.utils;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;

public class Utils {
	private static final Direction[] ALL_DIRECTIONS = Direction.values();
	private static final long INT_MASK = 0xFFFFFFFFL;
	
	public static Direction[] offsetDirection(Direction direction, int[] offset) {
		int ordinal = direction.ordinal();
		
		Direction[] directions = new Direction[offset.length];
		for (int i = 0; i < offset.length; i++) {
			directions[i] = getDirection(ordinal + offset[i]);
		}
		return directions;
	}
	
	public static Direction getDirection(int number) {
		return ALL_DIRECTIONS[Math.floorMod(number, 8)];
	}
	
	public static long combine(int a, int b) {
		return ((a & INT_MASK) << 32 | (b & INT_MASK));
	}
	
	public static int[] split(long variable) {
		return new int[] {
				(int) (variable >> 32),
				(int) (variable & INT_MASK)
		};
	}
	
	public static RobotInfo getClosestRobot(MapLocation location, RobotInfo[] robots) {
		int minDistance = Integer.MAX_VALUE;
		RobotInfo closestRobot = null;
		
		for(RobotInfo robot : robots) {
			int distance = robot.location.distanceSquaredTo(location);
			if(distance < minDistance) {
				minDistance = distance;
				closestRobot = robot;
			}
		}
		
		return closestRobot;
	}
	
	public static int log2(int n) {
		if (n < 0) throw new IllegalArgumentException();
		
		return 31 - Integer.numberOfLeadingZeros(n);
	}
	
	private Utils() {};
}
