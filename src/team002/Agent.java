//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;

import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import team002.modules.Map;
import team002.modules.Movement;
import team002.modules.Radio;

public abstract class Agent {	
	private static final int HISTORY_SIZE = 10;
	
	private RobotController controller;
	private Random random;
	private Movement movement;
	private Radio radio;
	private Map map;
	
	private int id;
	private Team team;
	
	private MapLocation spawnLocation;
	
	private RobotInfo motherArchon;
	int motherLastSeen;
	
	private Deque<RobotInfo> history;
	
	//================================================================================
	// Main logic
	//================================================================================
	
	/**
	 * Called once just after the robot is spawned.
	 * @param controller
	 */
	public void init(RobotController controller) {
		this.controller = controller;
		random = new Random(controller.getID());
		
		movement = new Movement(this);
		radio = new Radio(this);
		map = new Map(this);
		
		id = controller.getID();
		team = controller.getTeam();
		
		// It may happen that mother dies during birth and we never get to know her ;(
		motherArchon = trySenseRobot(id);
		motherLastSeen = controller.getRoundNum();
		if(controller.getType() != RobotType.ARCHON) {
			for (RobotInfo robot : controller.senseNearbyRobots(2, controller.getTeam())) {
				if(robot.type == RobotType.ARCHON) {
					motherArchon = robot;
					break;
				}
			}
		}
		
		spawnLocation = controller.getLocation();
		
		history = new LinkedList<RobotInfo>();
	}
	
	public final void nextRound() {
		onTurnBegin();
		makeDecisions().behave();
		onTurnEnd();
	}
	
	/**
	 * Called at the beginning of the agent's turn.<br>
	 * Very important system updates happen here.
	 */
	protected void onTurnBegin() {
		radio.listen();
		map.update();
	}
	
	/**
	 * Called at the end of the agent's turn, just before 
	 * <code>controller.yield()</code>.<br>
	 */
	protected void onTurnEnd() {
		if(history.size() >= HISTORY_SIZE)
			history.remove(0);
		
		history.add(trySenseRobot(id));
	};
	
	/**
	 * Core logic is supposed to sit here. Choose wisely.
	 * @return the behavior for the agent to act on.
	 */
	public abstract Behavior makeDecisions();
	
	//================================================================================
	// Helper methods
	//================================================================================
	
	protected RobotInfo getMotherArchon() {
		return motherArchon;
	}
	
	protected MapLocation getSpawnLocation() {
		return spawnLocation;
	}
	
	protected RobotInfo trySenseRobot(int id) {
		if(controller.canSenseRobot(id))
			try {
				return controller.senseRobot(id);
			} catch(GameActionException e) {
				// If we ever get here, life is very unfair
				e.printStackTrace();
			}
		return null;
	}
	
	protected RobotInfo[] senseEnemiesInRange() {
		return controller.senseNearbyRobots(controller.getLocation(), 
				controller.getType().attackRadiusSquared, team.opponent());
	}
	
	protected boolean tryShootSomething() {
		RobotInfo bestTarget = null;
		for(RobotInfo enemy : senseEnemiesInRange()) {
			if(bestTarget == null || compareTargets(enemy, bestTarget))
				bestTarget = enemy;
		}
		
		try {
			if(bestTarget != null) {
				controller.attackLocation(bestTarget.location);
				return true;
			}
			
		} catch(GameActionException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Comparision of two possible targets. This function is invoked
	 * when we're looking for someone to shoot at.
	 * 
	 * @param first the first possible target
	 * @param second the second possible target
	 * @return true if the first robot is a better candidate for being shot.
	 */
	protected boolean compareTargets(RobotInfo first, RobotInfo second) {
		return first.health < second.health;
	}
	
	//================================================================================
	// Module accessors
	//================================================================================
	
	public final RobotController getController() {
		return controller;
	}
	
	public final Random getRandom() {
		return random;
	}
	
	public final Deque<RobotInfo> getHistory() {
		return history;
	}
	
	public final Movement getMovement() {
		return movement;
	}
	
	public final Radio getRadio() {
		return radio;
	}
	
	public final Map getMap() {
		return map;
	}
	
	public final Agent getAgent() {
		return this;
	}
	
	/**
	 * The most elemental building block of agent's AI.<br>
	 * Each behavior should provide an implementation for one action
 	 * the agent can take.<br>
 	 * <br>
 	 * Decision making should be held out of the behaviors.
	 */
	public abstract class Behavior {
		public abstract void behave();
	}
}