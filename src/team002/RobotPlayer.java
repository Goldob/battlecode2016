//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002;

import battlecode.common.Clock;
import battlecode.common.RobotController;
import battlecode.common.RobotType;
import team002.agents.ArchonAgent;
import team002.agents.GuardAgent;
import team002.agents.ScoutAgent;
import team002.agents.SoldierAgent;
import team002.agents.TurretAgent;
import team002.agents.ViperAgent;

public class RobotPlayer {
	
	public static void run(RobotController controller) {
		Agent agent = craftAgent(controller.getType());
		
		agent.init(controller);
		while(true) {
			agent.nextRound();
			Clock.yield();
		}
	}
	
	private static Agent craftAgent(RobotType type) {
		switch (type) {
		case ARCHON:
			return new ArchonAgent();
		case GUARD:
			return new GuardAgent();
		case SCOUT:
			return new ScoutAgent();
		case SOLDIER:
			return new SoldierAgent();
		case TTM:
		case TURRET:
			return new TurretAgent();
		case VIPER:
			return new ViperAgent();
		default:
			return null;
		}
	}
}
