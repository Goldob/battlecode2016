//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002.agents;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Team;
import team002.Agent;
import team002.modules.Movement.MovementMode;

import static battlecode.common.RobotType.ARCHON;
import static team002.modules.Movement.MovementConstraint.*;
import static team002.utils.Utils.*;

public class ArchonAgent extends Agent {
	private ArchonBehavior BUILD_AND_WANDER;
	private ArchonBehavior ACTIVATE_UNITS;

	@Override
	public void init(RobotController controller) {
		super.init(controller);
		BUILD_AND_WANDER = new ArchonBehavior() {
			
			@Override
			public void coreReady() {
				RobotType construction = getNextConstruction();
				if(controller.hasBuildRequirements(construction)) {
					tryToBuild(construction);
				} else {
					getMovement().tryToMove(getDirection(getRandom().nextInt(8)), MovementMode.NORMAL,
							AVOID_FOOTSTEPS);
				}
			}
		};
		
		getController().addMatchObservation("This is a test");
		
		ACTIVATE_UNITS = new ArchonBehavior() {

			@Override
			public void coreReady() {
				RobotInfo closest = getClosestRobot(getController().getLocation(), 
						controller.senseNearbyRobots(ARCHON.sensorRadiusSquared, Team.NEUTRAL));
				if(closest != null) {
					if(getController().getLocation().isAdjacentTo(closest.location)) {
						tryToActivate(closest.location);
					} else {
						getMovement().tryToMove(getController().getLocation()
								.directionTo(closest.location), MovementMode.BUG, AVOID_FOOTSTEPS);
					}
				}
			}
		};
	}
	
	private RobotType getNextConstruction() {
		// TODO
		return RobotType.SOLDIER;
	}
	
	@Override
	public Behavior makeDecisions() {
		// TODO
		return BUILD_AND_WANDER;
	}
	
	protected boolean tryToFixSomething() {
		double maxDifference = 0.0;
		RobotInfo mostBroken = null;
		
		for (RobotInfo robot : getController().senseNearbyRobots(ARCHON.attackRadiusSquared, getController().getTeam())) {
			if (robot.maxHealth - robot.health > maxDifference) {
				mostBroken = robot;
				maxDifference = robot.maxHealth-robot.health;
			}
		}
		
		if(maxDifference > 0) {
			try {
				getController().repair(mostBroken.location);
				return true;
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	protected boolean tryToActivate(MapLocation location) {
		try {
			getController().activate(location);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	protected boolean tryToBuild(RobotType construction) {
		Direction direction = getDirection(getRandom().nextInt());
		if(getController().canBuild(direction, construction)) {
			try {
				getController().build(direction, construction);
				// TODO Broadcast initialization data
				return true;
			} catch (GameActionException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	private abstract class ArchonBehavior extends Behavior {
		
		public abstract void coreReady();
		
		@Override
		public final void behave() {
			tryToFixSomething();
			
			if(getController().isCoreReady()) {
				coreReady();
			}
		}
	}
}
