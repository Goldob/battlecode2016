//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002.agents;

import battlecode.common.RobotController;
import team002.Agent;

public class ScoutAgent extends Agent {
	private ScoutBehavior BAIT_ZOMBIES;
	private ScoutBehavior RECKON;
	
	public void init(RobotController controller) {
		super.init(controller);
		
		BAIT_ZOMBIES = new ScoutBehavior() {

			@Override
			public void coreReady() {
				// TODO Auto-generated method stub
			}
		};
		
		RECKON = new ScoutBehavior() {

			@Override
			public void coreReady() {
				// TODO Auto-generated method stub
			}
		};
	}
	
	protected void report() {
		// TODO
	}
	
	@Override
	public Behavior makeDecisions() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private abstract class ScoutBehavior extends Behavior {

		public abstract void coreReady();
		
		@Override
		public void behave() {
			report();
			
			if(getController().isCoreReady()) {
				coreReady();
			}
		}
	}
}
