//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002.modules;

import battlecode.common.Direction;
import battlecode.common.MapLocation;
import team002.Agent;
import team002.utils.Interval2D;

import static battlecode.common.GameConstants.*;
import static team002.utils.Locations.*;

public class Map {
	private MapLocation nw_corner;
	private MapLocation se_corner;
	
	private boolean top_fixed;
	private boolean right_fixed;
	private boolean bottom_fixed;
	private boolean left_fixed;
	
	public Map(Agent agent) {
		MapLocation location = agent.getController().getLocation();
		MapLocation offset = new MapLocation(MAP_MAX_WIDTH - 1, MAP_MAX_HEIGHT - 1);
		
		nw_corner = substract(location, offset);
		se_corner = add(location, offset);
	}
	
	public void update() {
		// TODO
	}
	
	public void fixBorder(MapLocation location, Direction direction) {
		switch(direction) {
		case NORTH:
			nw_corner = setY(nw_corner, location.y);
			top_fixed = true;
			break;
		case EAST:
			se_corner = setX(se_corner, location.x);
			right_fixed = true;
			break;
		case SOUTH:
			se_corner = setY(se_corner, location.y);
			bottom_fixed = true;
			break;
		case WEST:
			nw_corner = setX(nw_corner, location.x);
			left_fixed = true;
			break;
		case NORTH_EAST:
			fixBorder(location, Direction.NORTH);
			fixBorder(location, Direction.EAST);
			break;
		case NORTH_WEST:
			fixBorder(location, Direction.NORTH);
			fixBorder(location, Direction.WEST);
			break;
		case SOUTH_EAST:
			fixBorder(location, Direction.SOUTH);
			fixBorder(location, Direction.EAST);
			break;
		case SOUTH_WEST:
			fixBorder(location, Direction.SOUTH);
			fixBorder(location, Direction.WEST);
			break;
		
		default:
			break;
		}
	}
	
	public boolean isSizeFixed() {
		return top_fixed && right_fixed && bottom_fixed && left_fixed;
	}
	
	public Interval2D getArea() {
		return new Interval2D(nw_corner, se_corner);
	}
}