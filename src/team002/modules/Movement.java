//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002.modules;

import static team002.utils.Utils.*;

import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotInfo;
import team002.Agent;

public class Movement {
	private Agent agent;
	
	public Movement(Agent agent) {
		this.agent = agent;
	}
	
	public boolean tryToMove(Direction direction, MovementMode mode, MovementConstraint... constraints) {
		Direction[] offsetDirs = offsetDirection(direction, mode.getOffsets(agent.getController().getID() % 2 == 0));
		
		for(Direction offsetDir : offsetDirs) {
			if(isValid(offsetDir) && areConstraintsSatisfied(offsetDir, constraints)) {
				try {
					if(agent.getController().canMove(offsetDir)) {
						agent.getController().move(offsetDir);
					} else {
						agent.getController().clearRubble(offsetDir);
					}
				} catch(GameActionException e) {
					e.printStackTrace();
				}
				return true;
			}
		}
		
		return false;
	}
	
	private boolean areConstraintsSatisfied(Direction direction, MovementConstraint[] constraints) {
		for(MovementConstraint constraint : constraints) {
			if(!constraint.canMove(agent, direction))
				return false;
		}
		return true;
	}
	
	private boolean isValid(Direction direction) {
		MapLocation newLocation = agent.getController().getLocation().add(direction);
		try {
			return agent.getController().onTheMap(newLocation)
					&& !agent.getController().isLocationOccupied(newLocation);
		} catch (GameActionException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public enum MovementMode {
		NORMAL {

			@Override
			public int[] getOffsets(boolean leftFirst) {
				if(leftFirst) {
					return new int[]{0,1,-1,2,-2,3,-3,4};
				} else {
					return new int[]{0,-1,1,-2,2,-3,3,-4};
				}
			}
		},
		BUG {

			@Override
			public int[] getOffsets(boolean leftFirst) {
				if(leftFirst) {
					return new int[]{0,-1,-2,-3,-4,-5,-6,-7};
				} else {
					return new int[]{0,1,2,3,4,5,6,7};
				}
			}
		},
		FORWARD {
			
			@Override
			public int[] getOffsets(boolean leftFirst) {
				if(leftFirst) {
					return new int[]{0,1,-1};
				} else {
					return new int[]{0,-1,1};
				}
			}
		};
		
		public abstract int[] getOffsets(boolean leftFirst);
	}
	
	public static enum MovementConstraint {
		AVOID_FOOTSTEPS {
			
			@Override
			protected boolean canMove(Agent agent, Direction direction) {
				MapLocation newLocation = agent.getController().getLocation().add(direction);
				for (RobotInfo status : agent.getHistory()) {
					if(status.location.equals(newLocation))
							return false;
				}
				return true;
			}
		},
		AVOID_RUBBLE {

			@Override
			protected boolean canMove(Agent agent, Direction direction) {
				MapLocation newLocation = agent.getController().getLocation().add(direction);
				return agent.getController().senseRubble(newLocation) > GameConstants.RUBBLE_SLOW_THRESH;
			}	
		};
		
		protected abstract boolean canMove(Agent agent, Direction direction);
	}
}
