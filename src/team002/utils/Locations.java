//**********************************************************
// Team002 entry for Battlecode 2016
// Author: Adam Gotlib <Goldob>
//**********************************************************

package team002.utils;

import battlecode.common.MapLocation;

public class Locations {
	private Locations() {};
	
	public static MapLocation add(MapLocation a, MapLocation b) {
		return a.add(b.x, b.y);
	}
	
	public static MapLocation substract(MapLocation a, MapLocation b) {
		return a.add(-b.x, -b.y);
	}
	
	public static MapLocation setX(MapLocation location, int x) {
		return new MapLocation(x, location.x);
	}
	
	public static MapLocation setY(MapLocation location, int y) {
		return new MapLocation(location.x, y);
	}
}
