package team002.modules;

import static org.junit.Assert.*;
import org.junit.Test;

import battlecode.common.MapLocation;
import team002.modules.Radio.EncodedMessage;
import team002.modules.Radio.MessageType;

public class RadioTest {

	@Test
	public void encodedMessageTest() {
		EncodedMessage message = new EncodedMessage(MessageType.DEFAULT);

		short val1 = 1535;
		byte val2 =  23;
		MapLocation val3 = new MapLocation(1342,-653);

		message.setShort(8, val1);
		message.setByte(24, val2);
		message.setMapLocation(32, val3);
		
		
		assertEquals(val1, message.getShort(8));
		assertEquals(val2, message.getByte(24));
		assertEquals(val3, message.getMapLocation(32));
	}
}
