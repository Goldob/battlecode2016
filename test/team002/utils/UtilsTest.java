package team002.utils;

import java.util.Random;

import static org.junit.Assert.*;
import static team002.utils.Utils.*;

import org.junit.Before;
import org.junit.Test;

public class UtilsTest {
	private Random random;
	
	@Before
	public void setupEnvironment() {
		random = new Random();
	}
	
	@Test
	public void splitTest() {
		for (int i = 0; i < 100; i++) {
			long variable = random.nextLong();
			int[] splitted = split(variable);
			assertEquals(variable, combine(splitted[0], splitted[1]));
		}
	}
}
